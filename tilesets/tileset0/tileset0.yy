{
    "id": "0dc701b1-f451-4133-9883-86a53990eb5f",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tileset0",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 4,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "b4515e23-20df-4597-abfe-73f1b68ed95f",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 16,
    "tileheight": 16,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 16,
    "tilexoff": 0,
    "tileyoff": 0
}