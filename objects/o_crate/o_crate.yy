{
    "id": "8e180429-0250-4182-847b-ddfeec514548",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_crate",
    "eventList": [
        {
            "id": "4e4c12bd-4ede-4b26-8f62-449a808bcc89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8e180429-0250-4182-847b-ddfeec514548"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6f7d5177-3db8-4efd-aee1-2cc7c88ff0fe",
    "visible": true
}