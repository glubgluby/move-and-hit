if (instance_exists(follow))
{
	xTo = follow.x;
	yTo = follow.y;
}

//Update object position
x += round((xTo - x) / 15);
y += round((yTo - y) / 15);

//Keep camera center inside room
x = clamp(x,view_w_half+buff,room_width-view_w_half-buff);
y = clamp(y,view_h_half+buff,room_height-view_h_half-buff);

//Screen shake
x += irandom_range(-shake_remain,shake_remain)
y += irandom_range(-shake_remain,shake_remain)

shake_remain = max(0,shake_remain-((1/shake_length)*shake_magnitude));

//Update camera view
camera_set_view_pos(cam,x-view_w_half,y-view_h_half);

//grav shenaningans
if global.grav = 1 {
	camera_set_view_angle(cam,(lerp(camera_get_view_angle(cam),270,.1)));
}

if global.grav = 2{
	camera_set_view_angle(cam,(lerp(camera_get_view_angle(cam),180,.1)));
}

if global.grav = 3{
	camera_set_view_angle(cam,(lerp(camera_get_view_angle(cam),90,.1)));
}

if global.grav = 0 and (camera_get_view_angle(cam) > 180){
	
	camera_set_view_angle(cam,(lerp(camera_get_view_angle(cam),360,.1)));
}
if global.grav = 0 and (camera_get_view_angle(cam) < 180){
	camera_set_view_angle(cam,(lerp(camera_get_view_angle(cam),0,.1)));
}