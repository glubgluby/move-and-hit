cam = camera_create_view(-50,-50,768,768,0,o_player,-1,-1,128,128);

follow = o_player;
buff = 32;
view_w_half = camera_get_view_width(cam) * 0.5;
view_h_half = camera_get_view_height(cam) * 0.5;
xTo = xstart;
yTo = ystart;

shake_length = 0;
shake_magnitude = 0;
shake_remain = 0;

view_set_camera(1, cam);

view_visible[0] = false;
view_visible[1] = true;
view_enabled = true;


display_set_gui_size(768,768);
