{
    "id": "c87f7c7f-ffe3-4de5-ac1c-89b42285e487",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_wall",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "d201bbcb-8619-4ac3-b8dc-64bb62208b1b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "8bc6e6e3-047e-4bfd-8e9a-dc03861064e4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "c2179ac5-11af-4b3c-ad39-e4d628b0a0f3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "ce9e64ab-d2e7-4756-adbb-4d9b2db661e1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b2ba8f89-89d4-4cef-841e-bafd305efa5c",
    "visible": true
}