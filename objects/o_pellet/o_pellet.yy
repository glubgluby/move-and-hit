{
    "id": "b77fd420-7f14-49e2-897d-7a69c07cc4a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_pellet",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3f0d34ca-80dd-4847-b3bc-964c9bd36862",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d77ee61b-51c3-45d6-8f66-e9e212dffdb0",
    "visible": true
}