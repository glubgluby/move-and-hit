{
    "id": "6c702cf3-8382-40da-9446-9841ab65f850",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_warp",
    "eventList": [
        {
            "id": "cc17913b-4d50-4172-bca1-ceb5ccf69f43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6c702cf3-8382-40da-9446-9841ab65f850"
        },
        {
            "id": "13e9f1ad-2807-4068-9718-12fc4ce64e37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6c702cf3-8382-40da-9446-9841ab65f850"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3bf94c4e-2e53-4360-8104-8f9bad7150b9",
    "visible": true
}