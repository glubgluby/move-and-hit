///@description list of all guns/their statistics
persistent = true;
///starting gun:

//set gun 0 in the array
weapons[0] = ds_map_create();
//set sprite of base gun
ds_map_add(weapons[0],"sprite",s_gun);
//set recoil of weapon, 
//ideally this should make the gun tilt upward when fired. def should be a feature on revolver
//currently the gun is automatically updated wherever youre mouse is so it doesnt work
ds_map_add(weapons[0],"recoil",0);
//set how much the player is pushed back when when gun is fired this gun should have 0,
//but the chaingun should have so much you can barely walk forward
//doesnt seem to do anything currently because of course it doesnt
ds_map_add(weapons[0],"recoil_push",4);
//set damage of gun(currently stored in bullet so this does nothing)
ds_map_add(weapons[0],"damage",0);
//set the projectile object fired by the gun
ds_map_add(weapons[0],"projectile",o_bullet_gun);
//how long the gun waits between the trigger being pulled and the gun actually firing
ds_map_add(weapons[0],"startup",0);
//the length of the gun sprite, where the bullet should spawn
ds_map_add(weapons[0],"length",12);
//basically the speed at which you can fire the weapon(i think thats how i coded it)
ds_map_add(weapons[0],"cooldown",5);
//speed at which bullets move
ds_map_add(weapons[0],"bulletspeed",10);
//determines whether the gun automatic or not. Works!
ds_map_add(weapons[0],"automatic",false);
ds_map_add(weapons[0], "spread", 1);
ds_map_add(weapons[0],"weight",0);
ds_map_add(weapons[0],"sound",noone);
//chaingun
weapons[1] = ds_map_create();
ds_map_add(weapons[1],"sprite",s_chaingun);
ds_map_add(weapons[1],"recoil",1.5);
ds_map_add(weapons[1],"recoil_push",3);
ds_map_add(weapons[1],"damage",2);
ds_map_add(weapons[1],"projectile",o_bullet_gun);
ds_map_add(weapons[1],"startup",5);
ds_map_add(weapons[1],"length",14);
ds_map_add(weapons[1],"cooldown",2);
ds_map_add(weapons[1],"bulletspeed",12);
ds_map_add(weapons[1],"automatic",true);
ds_map_add(weapons[1], "spread", 3);
ds_map_add(weapons[1],"weight",2);
ds_map_add(weapons[1],"sound",noone);

//power gun
weapons[2] = ds_map_create();
ds_map_add(weapons[2],"sprite",s_double_gun);
ds_map_add(weapons[2],"recoil",0);
ds_map_add(weapons[2],"recoil_push",10);
ds_map_add(weapons[2],"damage",2);
ds_map_add(weapons[2],"projectile",o_bullet_gun);
ds_map_add(weapons[2],"startup",16);
ds_map_add(weapons[2],"length",16);
ds_map_add(weapons[2],"cooldown",10);
ds_map_add(weapons[2],"bulletspeed",20);
ds_map_add(weapons[2],"automatic",false);
ds_map_add(weapons[2], "spread", 1);
ds_map_add(weapons[2],"weight",0);
ds_map_add(weapons[2],"sound",noone);
// shotgun
weapons[3] = ds_map_create();
ds_map_add(weapons[3],"sprite",s_shotgun);
ds_map_add(weapons[3],"recoil",0);
ds_map_add(weapons[3],"recoil_push",10);
ds_map_add(weapons[3],"damage",3);
ds_map_add(weapons[3],"projectile",o_pellet);
ds_map_add(weapons[3],"startup",0);
ds_map_add(weapons[3],"length",38);
ds_map_add(weapons[3],"cooldown",15);
ds_map_add(weapons[3],"bulletspeed",20);
ds_map_add(weapons[3],"automatic",false);
ds_map_add(weapons[3],"spread", 10);
ds_map_add(weapons[3],"bullets", 8);
ds_map_add(weapons[3],"weight",0);
ds_map_add(weapons[3],"sound",Shotgun1);
//railgun
weapons[4] = ds_map_create();
ds_map_add(weapons[4],"sprite",s_railgun);
ds_map_add(weapons[4],"recoil",0);
ds_map_add(weapons[4],"recoil_push",15);
ds_map_add(weapons[4],"damage",6);
ds_map_add(weapons[4],"projectile",o_railgun);
ds_map_add(weapons[4],"startup",17);
ds_map_add(weapons[4],"length",40);
ds_map_add(weapons[4],"cooldown",30);
ds_map_add(weapons[4],"bulletspeed",0);
ds_map_add(weapons[4],"automatic",false);
ds_map_add(weapons[4],"spread",0);
ds_map_add(weapons[4],"bullets",1);
ds_map_add(weapons[4],"weight",7);
ds_map_add(weapons[4],"sound",noone);





WeaponChange(4);
current_cd = 0;
current_delay = -1;
current_recoil = 0;
