{
    "id": "c34f0162-17a4-4f72-9ec4-917e82905b6d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_gun",
    "eventList": [
        {
            "id": "b78ed46b-1e00-4fb8-8830-1b2f3f21e28c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c34f0162-17a4-4f72-9ec4-917e82905b6d"
        },
        {
            "id": "ee300d1e-d9ef-4659-849a-d40d666127ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c34f0162-17a4-4f72-9ec4-917e82905b6d"
        },
        {
            "id": "219bec3d-6ee2-4d35-ba88-3b77024b303b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "c34f0162-17a4-4f72-9ec4-917e82905b6d"
        },
        {
            "id": "a747a131-adba-41ca-9783-1a243275bbd9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c34f0162-17a4-4f72-9ec4-917e82905b6d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2c8b5fc1-85f9-4474-9860-477ffde086cb",
    "visible": true
}