//initialize variables
global.grav = 0;
global.gravx = 0.0;
global.gravy = 1.0;
global.player = self;
global.can_do1 = true;
global.can_do2 = true;
global.can_do3 = true;
global.can_do4 = true


gravi = .3
hsp = 0;
vsp = 0;
jumpspeed = 2;
movespeed = 2;
keyboard_set_map(ord("A"),vk_left)
keyboard_set_map(ord("W"),vk_up)
keyboard_set_map(ord("S"),vk_down)
keyboard_set_map(ord("D"),vk_right)
acceleration_ = 1
jumped = false;

//image scaling variables
xscale_ = image_xscale;
yscale_ = image_yscale;

image_speed = 0;

persistent = true;