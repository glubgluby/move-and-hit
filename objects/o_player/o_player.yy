{
    "id": "22af5e26-4f2a-4be6-a374-c180738b32bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_player",
    "eventList": [
        {
            "id": "7705bc2b-9f04-4ba0-87fa-65f008925412",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "22af5e26-4f2a-4be6-a374-c180738b32bb"
        },
        {
            "id": "4a003698-2c2d-4fd1-9300-dd10d367a45a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "22af5e26-4f2a-4be6-a374-c180738b32bb"
        },
        {
            "id": "d47b3dbb-0f3a-404d-a596-0fa94a1353ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "22af5e26-4f2a-4be6-a374-c180738b32bb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "e69eaf73-0f40-4390-b586-65559a340f0b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "f1f0e9e0-da66-443a-9140-63c4723e56f8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "67ab6ddf-2745-41cd-b2c4-631abb58e9a4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "372bad71-e435-4b5d-bfd9-95624df5326e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "59e1ba7a-d276-4f6a-9434-7343d81e0fcd",
    "visible": true
}