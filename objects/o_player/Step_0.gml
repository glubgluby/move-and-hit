//get player input

key_right = keyboard_check(vk_right);
key_left = -keyboard_check(vk_left);
key_jump = keyboard_check_pressed(vk_space);
grav_left = keyboard_check_released(ord("Q"));
grav_right = keyboard_check_released(ord("E"));
key_down = keyboard_check_pressed(vk_down);
grav_up = keyboard_check_pressed(vk_up);

//RESETTING IMAGE SCALE
xscale_ = lerp(xscale_,image_xscale, .1);
yscale_ = lerp(yscale_,image_yscale, .1);

//reading input
//key_left either equals -1 or 0, key right = either 1 or 0, therefore it moves left or right

move = key_left + key_right;




//gravity flipping enable or disable

if (grav_left = 1){
	gravflip();
	global.can_do1 = false;
} if (grav_right = 1){
	gravflip();
	global.can_do2 = false;
}


//refresh your abilities when landing
if on_ground(){
	global.can_do1 = true
	global.can_do2 = true
	global.can_do3 = true
	global.can_do4 = true
}

if grav_up == 1{
	show_debug_message("fwip")
	grav_vertical();
}

//downard dash mechanic
dashdown();

//image changes when you jump
if keyboard_check_pressed(vk_space) and on_ground() {
	xscale_ = image_xscale*.8;
	yscale_ = image_yscale*1.4;
	show_debug_message("boing")
}

 
physics_move()

//set image to move when you land
if global.grav = 0{
	if place_meeting(x,y+1, o_wall) && !place_meeting(x,yprevious+1, o_wall){
		global.can_do1 = true
		global.can_do2 = true
		global.can_do3 = true
		global.can_do4 = true
		xscale_ = image_xscale*1.4;
		yscale_ = image_yscale*.8;
	}
}
if global.grav = 1{
	if place_meeting(x+1,y, o_wall) && !place_meeting(xprevious+1,y, o_wall){
		global.can_do1 = true
		global.can_do2 = true
		global.can_do3 = true
		global.can_do4 = true
		xscale_ = image_xscale*1.4;
		yscale_ = image_yscale*.8;
	}
}
if global.grav = 2{
	if place_meeting(x,y-1, o_wall) && !place_meeting(x,yprevious-1, o_wall){
		global.can_do1 = true
		global.can_do2 = true
		global.can_do3 = true
		global.can_do4 = true
		xscale_ = image_xscale*1.4;
		yscale_ = image_yscale*.8;
	}
}
if global.grav = 3{
	if place_meeting(x-1,y, o_wall) && !place_meeting(xprevious-1,y, o_wall){
		global.can_do1 = true
		global.can_do2 = true
		global.can_do3 = true
		global.can_do4 = true
		xscale_ = image_xscale*1.4;
		yscale_ = image_yscale*.8;
	}
}

if place_meeting(x,y,o_crate){
	{	
		with(o_gun){
			WeaponChange(round(random_range(.6,4.5)));
			show_debug_message(weapon);
		} 
	instance_destroy(o_crate);
	}
}