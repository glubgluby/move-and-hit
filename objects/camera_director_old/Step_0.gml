/// @description Insert description here
// You can write your code in this e
if instance_exists(o_player)
camera_set_view_pos(camera, o_player.x - camera_get_view_width(camera)/2,
o_player.y - camera_get_view_height(camera)/2);

if global.grav = 1 {
	camera_set_view_angle(camera,(lerp(camera_get_view_angle(camera),270,.1)));
}

if global.grav = 2{
	camera_set_view_angle(camera,(lerp(camera_get_view_angle(camera),180,.1)));
}

if global.grav = 3{
	camera_set_view_angle(camera,(lerp(camera_get_view_angle(camera),90,.1)));
}

if global.grav = 0 and (camera_get_view_angle(camera) > 180){
	
	camera_set_view_angle(camera,(lerp(camera_get_view_angle(camera),360,.1)));
}
if global.grav = 0 and (camera_get_view_angle(camera) < 180){
	camera_set_view_angle(camera,(lerp(camera_get_view_angle(camera),0,.1)));
}