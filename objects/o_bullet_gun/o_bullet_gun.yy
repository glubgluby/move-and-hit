{
    "id": "3f0d34ca-80dd-4847-b3bc-964c9bd36862",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_bullet_gun",
    "eventList": [
        {
            "id": "94f5c73a-cb99-4a63-9859-a8ccb9f71832",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3f0d34ca-80dd-4847-b3bc-964c9bd36862"
        },
        {
            "id": "44d148ef-d29e-44ac-b5ea-ffa45f2c85cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c87f7c7f-ffe3-4de5-ac1c-89b42285e487",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3f0d34ca-80dd-4847-b3bc-964c9bd36862"
        },
        {
            "id": "2e7fdca3-85b6-4fc3-862a-10f19ded09c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e282f925-8d6c-4581-b462-9e99e28918ee",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3f0d34ca-80dd-4847-b3bc-964c9bd36862"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e2b4d3ca-703f-4976-bbac-094783199e33",
    "visible": true
}