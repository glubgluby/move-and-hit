{
    "id": "4e64e7d6-593a-4a88-9a0f-59073d4d0a83",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_floater",
    "eventList": [
        {
            "id": "4b579fcc-e631-40b3-ae60-e1edf8fb73ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4e64e7d6-593a-4a88-9a0f-59073d4d0a83"
        },
        {
            "id": "d01c3320-5a21-4d76-962d-db0454184dcc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4e64e7d6-593a-4a88-9a0f-59073d4d0a83"
        },
        {
            "id": "b00a64a6-6671-4be2-9684-cd378408bbbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4e64e7d6-593a-4a88-9a0f-59073d4d0a83"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e282f925-8d6c-4581-b462-9e99e28918ee",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "d7a1955b-b0a1-4724-9445-8661b0ec41e1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "d4e9bf1f-7538-40d6-8787-1480dc77c789",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 25,
            "y": 0
        },
        {
            "id": "8807ea9f-2c3d-4d79-99ef-095d731f2a2c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 25,
            "y": 25
        },
        {
            "id": "f268d2e9-8eaa-45f1-881a-0f2077c8bab0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 25
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e2e58c76-54c5-4db0-8f46-5945c05a7c0d",
    "visible": true
}