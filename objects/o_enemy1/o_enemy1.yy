{
    "id": "e282f925-8d6c-4581-b462-9e99e28918ee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_enemy1",
    "eventList": [
        {
            "id": "05d889b9-99b2-466c-9514-7a719607c708",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e282f925-8d6c-4581-b462-9e99e28918ee"
        },
        {
            "id": "f56c85a0-d2d6-4a26-a8f8-5ef1366a0969",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e282f925-8d6c-4581-b462-9e99e28918ee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "4a03f661-1484-4c83-bc28-5ac682b29bba",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "cbc5b0f4-e483-4a58-b914-7c8c2b0c8fc6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 25,
            "y": 0
        },
        {
            "id": "f6c82283-8e83-4ae6-9062-ce797f8fc0df",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 25,
            "y": 25
        },
        {
            "id": "dfa69b3f-d57c-40e8-a76e-fa5a9d4fb89e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 25
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f4d01d62-f8a8-4a99-812c-a2baa1d4befc",
    "visible": true
}