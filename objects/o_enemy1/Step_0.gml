/// gravi = .3
key_jump = 0;
if (hsp = 0 and vsp = 0) or irandom(100) = 0 {
	key_jump = min(irandom(5) - 1, 1);
	//move = clamp((global.player.x - x) * (1 - global.gravx) + (global.player.y - y) * (1 - global.gravy), -1, 1)
	move = irandom(2) - 1;
}
physics_move();
xscale_ = lerp(xscale_,image_xscale, .1);
yscale_ = lerp(yscale_,image_yscale, .1);

//set image to move when you land
if global.grav = 0{
	if place_meeting(x,y+1, o_wall) && !place_meeting(x,yprevious+1, o_wall
	){
		xscale_ = image_xscale*1.4;
		yscale_ = image_yscale*.8;
	}
}
if global.grav = 1{
	if place_meeting(x+1,y, o_wall) && !place_meeting(xprevious+1,y, o_wall){
		xscale_ = image_xscale*1.4;
		yscale_ = image_yscale*.8;
	}
}
if global.grav = 2{
	if place_meeting(x,y-1, o_wall) && !place_meeting(x,yprevious-1, o_wall){
		xscale_ = image_xscale*1.4;
		yscale_ = image_yscale*.8;
	}
}
if global.grav = 3{
	if place_meeting(x-1,y, o_wall) && !place_meeting(xprevious-1,y, o_wall){
		xscale_ = image_xscale*1.4;
		yscale_ = image_yscale*.8;
	}
}

//wow your bullets are really fast

//kill me to death
if hp_ <= 0 {
	instance_destroy();
}

if hsp != 0 {
	sprite_index = s_enemyrun;
	image_speed = .3;
} else {
	sprite_index = s_enemyidle
	image_speed = .3;
}

if global.grav = 0{
	image_angle = 0;
}
if global.grav = 1{
	image_angle = 90;
} 
if global.grav = 2{
	image_angle = 180;
}
if global.grav = 3{
	image_angle = 270;
}

if point_direction(x,y,o_player.x,o_player.y) > 200 {
	
}