if (!scan)
{
	var range = 1000
	show_debug_message(string(direction));
	array = hitscan(x,y,x+lengthdir_x(range,direction),y+lengthdir_y(range,direction),o_wall,false,false);
	scan = true;
	if (array[0] != noone)
	{

		//with(instance_create_layer(array[1],array[2],"Projectiles",oHit)) image_angle = other.direction;
	}
}
else
{
	with collision_line(x, y, array[1], array[2], o_enemy1, false, false) {
		hp_ -= 10;
	}
	image_alpha = image_alpha - 0.1;
	if (image_alpha < 0.1) instance_destroy();
}