{
    "id": "354934ec-b07d-444b-86c3-1842adfae04c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_railgun",
    "eventList": [
        {
            "id": "d36b1fe1-6283-47b3-b4ac-c18b4a32a8cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "354934ec-b07d-444b-86c3-1842adfae04c"
        },
        {
            "id": "c54f24c9-43c5-42f6-a04f-d231067d9046",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "354934ec-b07d-444b-86c3-1842adfae04c"
        },
        {
            "id": "c6102436-fc66-48e0-8bd5-f62bf96f620e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "354934ec-b07d-444b-86c3-1842adfae04c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f7f2252d-c5f0-4f2d-a5f2-b72f10f264d7",
    "visible": true
}