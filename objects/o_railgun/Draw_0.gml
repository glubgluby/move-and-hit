/// @desc ?
if (scan)
{
	draw_set_alpha(image_alpha);
	if (array[0] == noone) {
		draw_set_colour(c_red);
	}
	draw_line_width_colour(x,y,array[1],array[2],2,c_gray,c_white);
	var d = point_distance(x,y,array[1],array[2]);
	draw_sprite_ext(sprite_index,image_index,x,y,d/sprite_width,2,direction,c_white,image_alpha);
	
	draw_set_alpha(1);
}
