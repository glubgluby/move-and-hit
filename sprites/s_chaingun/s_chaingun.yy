{
    "id": "b3266a5f-f382-4148-a718-7cfb851908ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_chaingun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e22d850d-b719-4b9a-ac45-838bc2490c35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3266a5f-f382-4148-a718-7cfb851908ea",
            "compositeImage": {
                "id": "59778a98-cd30-4de6-8a59-639794a2981a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e22d850d-b719-4b9a-ac45-838bc2490c35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9da06ace-d21d-4e2c-94d9-72b953f0eb78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e22d850d-b719-4b9a-ac45-838bc2490c35",
                    "LayerId": "4a9fa377-79a1-4808-81c9-140f80d94acd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "4a9fa377-79a1-4808-81c9-140f80d94acd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3266a5f-f382-4148-a718-7cfb851908ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 3,
    "yorig": 8
}