{
    "id": "99e37489-2e99-4859-9891-ff79263782a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_double_gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 25,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6014b1f8-15c0-4289-9ec9-bcce80e478db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99e37489-2e99-4859-9891-ff79263782a3",
            "compositeImage": {
                "id": "3aa1936f-8885-490d-a46b-e20565dea7ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6014b1f8-15c0-4289-9ec9-bcce80e478db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea4ee05a-522e-4355-8065-cdbe98e77009",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6014b1f8-15c0-4289-9ec9-bcce80e478db",
                    "LayerId": "c2769fdb-2456-4ee2-a163-e1876c9aced6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c2769fdb-2456-4ee2-a163-e1876c9aced6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99e37489-2e99-4859-9891-ff79263782a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 7,
    "yorig": 9
}