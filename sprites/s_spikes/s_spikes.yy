{
    "id": "ab435b53-890e-441a-b13d-fc0d95bf51a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_spikes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d6a37aa-fc87-44f9-b6f3-f612f2b0ef34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab435b53-890e-441a-b13d-fc0d95bf51a4",
            "compositeImage": {
                "id": "8321e39e-4d22-4518-bb1c-895837c18b35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d6a37aa-fc87-44f9-b6f3-f612f2b0ef34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74779e98-2789-4739-8616-8171ce7801d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d6a37aa-fc87-44f9-b6f3-f612f2b0ef34",
                    "LayerId": "342d2b8e-8aec-4b61-98c2-9654c3300894"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "342d2b8e-8aec-4b61-98c2-9654c3300894",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab435b53-890e-441a-b13d-fc0d95bf51a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}