{
    "id": "b4515e23-20df-4597-abfe-73f1b68ed95f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_tilesettest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ceb1217a-4e85-4ac4-bb92-5c4a1e5d1b80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4515e23-20df-4597-abfe-73f1b68ed95f",
            "compositeImage": {
                "id": "24c0f0cd-2a90-4fc6-a23f-59b72f2f7826",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ceb1217a-4e85-4ac4-bb92-5c4a1e5d1b80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63ea69e5-fee5-4896-b7a2-8c86980b566d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ceb1217a-4e85-4ac4-bb92-5c4a1e5d1b80",
                    "LayerId": "7c12cdb5-d091-491d-82c1-41ed47219e91"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7c12cdb5-d091-491d-82c1-41ed47219e91",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4515e23-20df-4597-abfe-73f1b68ed95f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}