{
    "id": "6f7d5177-3db8-4efd-aee1-2cc7c88ff0fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_crate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d676999-9e3c-4db1-a866-de3bdab83ccb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f7d5177-3db8-4efd-aee1-2cc7c88ff0fe",
            "compositeImage": {
                "id": "3f348725-8d7d-407f-96e2-4901d1c614f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d676999-9e3c-4db1-a866-de3bdab83ccb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bac60797-a2df-484c-8eff-c3f96836359c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d676999-9e3c-4db1-a866-de3bdab83ccb",
                    "LayerId": "26129768-4a23-4d45-84d9-51c3735effbe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "26129768-4a23-4d45-84d9-51c3735effbe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f7d5177-3db8-4efd-aee1-2cc7c88ff0fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}