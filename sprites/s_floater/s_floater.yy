{
    "id": "e2e58c76-54c5-4db0-8f46-5945c05a7c0d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_floater",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 3,
    "bbox_right": 17,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68ca660f-1e2b-4a45-8762-4b5fa6fee7a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2e58c76-54c5-4db0-8f46-5945c05a7c0d",
            "compositeImage": {
                "id": "bde82950-cd8a-4dc6-80f6-612a402bd9ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68ca660f-1e2b-4a45-8762-4b5fa6fee7a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a64545fc-3fbd-4d1d-b618-52e3b06f3b2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68ca660f-1e2b-4a45-8762-4b5fa6fee7a2",
                    "LayerId": "0648654a-eed0-4aef-88a2-f40aaae3ab0b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "0648654a-eed0-4aef-88a2-f40aaae3ab0b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2e58c76-54c5-4db0-8f46-5945c05a7c0d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}