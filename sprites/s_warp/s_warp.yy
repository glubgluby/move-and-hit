{
    "id": "3bf94c4e-2e53-4360-8104-8f9bad7150b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_warp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 16,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d35f720d-ec13-494b-843c-f5eb2ff587bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bf94c4e-2e53-4360-8104-8f9bad7150b9",
            "compositeImage": {
                "id": "4142a932-a33a-42aa-8378-f56dd7ee0660",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d35f720d-ec13-494b-843c-f5eb2ff587bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2f57074-1a3c-4ff2-92b0-1df78b3e9b98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d35f720d-ec13-494b-843c-f5eb2ff587bd",
                    "LayerId": "bd38faf6-5e3f-4820-813a-69f6214c61ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bd38faf6-5e3f-4820-813a-69f6214c61ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3bf94c4e-2e53-4360-8104-8f9bad7150b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}