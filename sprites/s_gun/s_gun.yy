{
    "id": "2c8b5fc1-85f9-4474-9860-477ffde086cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d6d946e-76eb-4dc8-87c0-37f20464fe3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c8b5fc1-85f9-4474-9860-477ffde086cb",
            "compositeImage": {
                "id": "69c7a704-bafb-49ba-89f1-acbb998d74f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d6d946e-76eb-4dc8-87c0-37f20464fe3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85d73d65-f692-49c8-b306-03c96072347a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d6d946e-76eb-4dc8-87c0-37f20464fe3b",
                    "LayerId": "823dea69-24c6-427b-9ffa-3209ef6bde1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "823dea69-24c6-427b-9ffa-3209ef6bde1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c8b5fc1-85f9-4474-9860-477ffde086cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 3,
    "yorig": 9
}