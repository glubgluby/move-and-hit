{
    "id": "e2b4d3ca-703f-4976-bbac-094783199e33",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bullet_gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 1,
    "bbox_right": 8,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4982bd5b-d3a7-433a-ba1d-6edd3e08431d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2b4d3ca-703f-4976-bbac-094783199e33",
            "compositeImage": {
                "id": "88afbd15-cd71-4db8-8cae-ae9e83ddd577",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4982bd5b-d3a7-433a-ba1d-6edd3e08431d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3a08e95-685a-4333-9ead-842f98f311a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4982bd5b-d3a7-433a-ba1d-6edd3e08431d",
                    "LayerId": "886002c3-181e-4b64-b4fe-87153c1684d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "886002c3-181e-4b64-b4fe-87153c1684d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2b4d3ca-703f-4976-bbac-094783199e33",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 4,
    "yorig": 4
}