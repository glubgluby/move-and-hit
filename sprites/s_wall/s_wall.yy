{
    "id": "b2ba8f89-89d4-4cef-841e-bafd305efa5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b77ada2-191c-4d11-9bca-c59df54221c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2ba8f89-89d4-4cef-841e-bafd305efa5c",
            "compositeImage": {
                "id": "5e4839e4-df96-4b3a-810e-5918817477e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b77ada2-191c-4d11-9bca-c59df54221c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1458aa30-a0be-4283-8384-d0982ae454af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b77ada2-191c-4d11-9bca-c59df54221c6",
                    "LayerId": "6622ddf2-2161-46c0-aaf4-781aa4d9147f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6622ddf2-2161-46c0-aaf4-781aa4d9147f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2ba8f89-89d4-4cef-841e-bafd305efa5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}