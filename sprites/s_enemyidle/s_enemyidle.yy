{
    "id": "f4d01d62-f8a8-4a99-812c-a2baa1d4befc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_enemyidle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8db98766-4b71-4724-8eaa-b4ff66e21c78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d01d62-f8a8-4a99-812c-a2baa1d4befc",
            "compositeImage": {
                "id": "d368f2db-a2df-4d3c-97e2-fba76c0b6f0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8db98766-4b71-4724-8eaa-b4ff66e21c78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6761faa-2b49-4adc-9491-dddfba29fa8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8db98766-4b71-4724-8eaa-b4ff66e21c78",
                    "LayerId": "14ea422d-912f-49a9-8388-6d6e7977a4db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "14ea422d-912f-49a9-8388-6d6e7977a4db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4d01d62-f8a8-4a99-812c-a2baa1d4befc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}