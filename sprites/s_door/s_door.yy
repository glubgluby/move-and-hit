{
    "id": "73e6bcf7-6b2e-4726-bc91-1c58a7352ffe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c359c11-86e6-4f89-8076-d7c85c4ad5d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73e6bcf7-6b2e-4726-bc91-1c58a7352ffe",
            "compositeImage": {
                "id": "10fd1b45-d357-489c-a1f0-021148c8d8dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c359c11-86e6-4f89-8076-d7c85c4ad5d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cedc49c-eb28-42ce-885f-0d2ff8cd7705",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c359c11-86e6-4f89-8076-d7c85c4ad5d2",
                    "LayerId": "ce4d2956-26d8-4a94-9b48-341ec829912e"
                }
            ]
        },
        {
            "id": "45c93795-6091-4918-ba1b-cf0eadabf3a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73e6bcf7-6b2e-4726-bc91-1c58a7352ffe",
            "compositeImage": {
                "id": "a3150284-75d6-48bf-87c1-6ba9df52698f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45c93795-6091-4918-ba1b-cf0eadabf3a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "490eece2-29c5-472a-984b-7d4b4440f967",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45c93795-6091-4918-ba1b-cf0eadabf3a3",
                    "LayerId": "ce4d2956-26d8-4a94-9b48-341ec829912e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ce4d2956-26d8-4a94-9b48-341ec829912e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73e6bcf7-6b2e-4726-bc91-1c58a7352ffe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}