/// @description gravity flipping
//flipping gravity
//grav 0 is normal, grav 1, walking on right wall, grav 2 walking on ceiling, grav 3 walking on left wall
if grav_right == 1 and global.can_do2 == false{
	exit;
}
if grav_left == 1 and global.can_do1 == false{
	exit;
}
if grav_right = 1 and global.grav = 0{
	global.grav = 1;
	global.gravx = 1.0;
	global.gravy = 0.0;
	camera_set_view_angle(view_camera[1],359);
	show_debug_message("grav = 1");
	exit;
} if grav_left = 1 and global.grav = 1{
	global.grav = 0;
	global.gravx = 0.0;
	global.gravy = 1.0;
	show_debug_message("grav = 0");
	exit;
} if grav_left = 1 and global.grav = 0{
	global.grav = 3;
	global.gravx = -1.0;
	global.gravy = 0.0;
	camera_set_view_angle(view_camera[1],1);
	show_debug_message("grav = 3");
	exit;
} if grav_right = 1 and global.grav = 1{
	global.grav = 2;
	global.gravx = 0.0;
	global.gravy = -1.0;
	show_debug_message("grav = 2");
	exit;
} if grav_right = 1 and global.grav = 2{
	global.grav = 3;
	global.gravx = -1.0;
	global.gravy = 0.0;
	show_debug_message("grav = 3");
	exit;
} if grav_left = 1 and global.grav = 2{
	global.grav = 1;
	global.gravx = 1.0;
	global.gravy = 0.0;
	show_debug_message("grav = 1");
	exit;
} if grav_left = 1 and global.grav = 3{
	global.grav = 2;
	global.gravx = 0.0;
	global.gravy = -1.0;
	show_debug_message("grav = 2");
	exit;
} if grav_right = 1 and global.grav = 3{
	global.grav = 0;
	global.gravx = 0.0;
	global.gravy = 1.0;
	show_debug_message("grav = 0");
	exit;
}
