/// @desc hitscan(x1,y1,x2,y2,object,prec,notme)
//
//  Returns the instance id of an object colliding with a given line and
//  closest to the first point, or noone if no instance found.
// 
//
/// @arg x1
/// @arg y1
/// @arg x2
/// @arg y2
/// @arg object
/// @arg prec
/// @arg notme
{
    var x1, y1, x2, y2, object, prec, notme, sx, sy, inst, i, angle;
    x1 = argument0;
    y1 = argument1;
    x2 = argument2;
    y2 = argument3;
    object = argument4;
    prec = argument5;
    notme = argument6;
    sx = x2 - x1;
    sy = y2 - y1;
	angle = point_direction(x1, y1, x2, y2);
	draw_line(x1, y1, x2, y2);
    inst = collision_line(x1, y1, x2 ,y2,object,prec,notme);
    if (inst != noone) {
        while ((abs(sx) >= 1) || (abs(sy) >= 1)) {
            sx /= 2;
            sy /= 2;
            i = collision_line(x1, y1, x2, y2, object, prec, notme);
            if (i) {
                x2 -= sx;
                y2 -= sy;
                inst = i;
            }else{
                x2 += sx;
                y2 += sy;
            }
        }
    
		while (collision_point(x2,y2,object,prec,notme) != noone)
		{
			x2 -= lengthdir_x(1,angle);
			y2 -= lengthdir_y(1,angle);
		}
	}
	
	var array = [inst,x2,y2];
    return array;
}
