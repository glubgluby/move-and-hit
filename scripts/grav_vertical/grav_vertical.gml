if !global.can_do4 {
	exit;
}
if global.grav == 0{
	global.can_do4 = false
	global.grav = 2;
	global.gravx = 0.0;
	global.gravy = -1.0;
	show_debug_message("grav = 2");
	exit;
} if global.grav == 2{
	global.can_do4 = false
	global.grav = 0;
	global.gravx = 0.0;
	global.gravy = 1.0;
	show_debug_message("grav = 0");
	exit;
} if global.grav == 1{
	global.can_do4 = false
	global.grav = 3;
	global.gravx = -1.0;
	global.gravy = 0.0;
	show_debug_message("grav = 3");
	exit;
} if global.grav == 3{
	global.can_do4 = false
	global.grav = 1;
	global.gravx = 1.0;
	global.gravy = 0.0;
	show_debug_message("grav = 1")
	exit;	
}
	