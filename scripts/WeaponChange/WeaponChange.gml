weapon = argument0;
var wp_map = weapons[weapon];
sprite = ds_map_find_value(wp_map, "sprite");
recoil = wp_map[? "recoil"];
recoil_push = wp_map[? "recoil_push"];
damage = wp_map[? "damage"];
projectile = wp_map[? "projectile"];
startup = wp_map[? "startup"];
bulletspeed = wp_map[? "bulletspeed"];
automatic = wp_map[? "automatic"];
cooldown = wp_map[? "cooldown"];
length = wp_map[? "length"];
spread = wp_map[? "spread"];
bullets = wp_map[? "bullets"];
if is_undefined(bullets) {
	bullets = 1;
}
weight = wp_map[? "weight"];
sound = wp_map[? "sound"];
